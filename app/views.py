from app import app
from flask import render_template

page = None;

@app.route('/')
@app.route('/index')
def index():
	page = "index"
	return render_template('index.html', page=page)

@app.route('/about_me')
def about_me():	
	page = "about_me"
	return render_template('about-me.html', page=page)

@app.route('/contact_me')
def contact_me():	
	page = "contact_me"
	return render_template('contact-me.html', page=page)

@app.route('/projects')
def projects():
	page = "projects"
	return render_template('projects.html', page=page)

@app.route('/school')
def school():
	page = "school"
	return render_template('school.html', page=page)

@app.route('/work_experience')
def work_experience():
	page = "work_experience"
	return render_template('work-experience.html', page=page)